#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

void internal_semUnlink(){
	disastrOS_debug("[%d]semUnlink: starting\n",disastrOS_getpid());
	// retrieving parameters
 	int id=running->syscall_args[0];

  	// find the sempahore by the id
  	Semaphore* sem=SemaphoreList_byId(&semaphores_list, id);
	if (! sem){
	    //if des is not found there is a problem, the semaphore that have to be close does not exists
	    disastrOS_debug("[%d]semUnlink: the semaphore with id = %d does not exist\n",disastrOS_getpid(),id);
	    running->syscall_retvalue=DSOS_ESEMAPHORECLOSE;
	    return;
	}

	// ensure the resource is not used by any process
	if(sem->descriptors.size){
	    disastrOS_debug("[%d]semUnlink: the semaphore with id = %d is used by someone so I won't destroy it\n",disastrOS_getpid(),id);
	    sem->id = DSOS_SEM_ID_REMOVED;
	    running->syscall_retvalue=0;
	    return;
	}

	//deleting sem from semaphore_list and realising memory
	sem=(Semaphore*) List_detach(&semaphores_list, (ListItem*) sem);
	assert(sem);
	Semaphore_free(sem);
	running->syscall_retvalue=0;
	disastrOS_debug("[%d]semUnlink: the semaphore with id = %d has been destroyed\n",disastrOS_getpid(),id);
}
