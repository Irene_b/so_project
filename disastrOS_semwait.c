#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

void internal_semWait(){
	disastrOS_debug("[%d]semWait: starting\n",disastrOS_getpid());
	// retrieving parameters
  	int id = running->syscall_args[0];

  	SemDescriptor* des = SemDescriptorList_byFd(&running->sem_descriptors, id);
	// if des is not found there is a problem, the semaphore that have to be use does not exists 
  	if(! des){
			disastrOS_debug("[%d]semWait: semaphore with id = %d does not exist\n",disastrOS_getpid(),id);
		  	running->syscall_retvalue = DSOS_ESEMAPHOREWAIT;
		  	return;
  	}

	Semaphore* sem = des->semaphore;
	assert(sem);

	//decrementing the semaphore counter
	sem->count--;
	// if the semaphore counter is negative the processs will be quequed
	if(sem->count >= 0){
		running->syscall_retvalue = 0;
	}else{
	  	running->status=Waiting;
		SemDescriptorPtr* des_ptr = (SemDescriptorPtr*) List_detach(&sem->descriptors, (ListItem*) des->ptr);
	  	List_insert(&sem->waiting_descriptors, sem->waiting_descriptors.last, (ListItem*) des_ptr);
	  	List_insert(&waiting_list, waiting_list.last, (ListItem*) running);
		assert(ready_list.first);
		PCB* new_pcb = (PCB*) List_detach(&ready_list, ready_list.first);
		running->syscall_retvalue=new_pcb->pid;
	  	running = new_pcb;
		disastrOS_debug("[%d]semWait: new PCB running pid = %d\n",disastrOS_getpid(),running!=0?running->pid:-1);
		sem->count++;
	}
	disastrOS_debug("[%d]semWait: finished\n",disastrOS_getpid());
}
