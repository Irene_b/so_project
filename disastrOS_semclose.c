#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

void internal_semClose(){
	disastrOS_debug("[%d]semClose: starting\n",disastrOS_getpid());
	// retrieving parameters
  	int id = running->syscall_args[0];

  	SemDescriptor* des = SemDescriptorList_byFd(&running->sem_descriptors, id);
	//if des is not found there is a problem, the semaphore that have to be close does not exists 
  	if(! des){
		disastrOS_debug("[%d]semClose: the semaphore with id = %d does not exist\n",disastrOS_getpid(),id);
	  	running->syscall_retvalue=DSOS_ESEMAPHORECLOSE;
	  	return;
  	}

	// deleting des, des_ptr and sem from their respective lists
  	des = (SemDescriptor*) List_detach(&running->sem_descriptors, (ListItem*) des);
	assert(des);
  	Semaphore* sem = des->semaphore;
  	SemDescriptorPtr* des_ptr = (SemDescriptorPtr*) List_detach(&sem->descriptors, (ListItem*)(des->ptr));
	assert(des_ptr);

	if(sem->id == DSOS_SEM_ID_REMOVED && sem->descriptors.size == 0){
		disastrOS_debug("[%d]semClose: someone call semUnlink for the semaphore with id = %d so I destroy it\n",disastrOS_getpid(),id);
    		sem=(Semaphore*) List_detach(&semaphores_list, (ListItem*) sem);
		assert(sem);
  		Semaphore_free(sem);
  	}

	// releasing the memory
	SemDescriptor_free(des);
  	SemDescriptorPtr_free(des_ptr);
	disastrOS_debug("[%d]semClose: closed the semaphore with id = %d \n",disastrOS_getpid(),id);
  	running->syscall_retvalue = 0;
}
