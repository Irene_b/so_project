#include <stdio.h>
#include <unistd.h>
#include <poll.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

#define N 10// number of threads
#define M 10 // number of iterations per thread
#define V 1 // value added to the balance by each thread at each iteration

unsigned long int shared_variable;
int n = N, m = M, v = V;
int id = 102;
int mode=DSOS_CREATE|DSOS_EXCL;
unsigned int value = 1;
int result[4] = {0,0,0,0};
int res_1 = 0;
int res_2 = 0;

// we need this to handle the sleep state
void sleeperFunction(void* args){
  printf("Hello, I am the sleeper, and I sleep %d\n",disastrOS_getpid());
  while(1) {
    getc(stdin);
    disastrOS_printStatus();
  }
}


void thread_work(void* args) {
	int i;
	int sem = disastrOS_semOpen(id, DSOS_CREATE, 0);
	for (i = 0; i < m; i++) {
		disastrOS_semWait(sem);
		shared_variable += v;
		disastrOS_sleep(2);
		disastrOS_semPost(sem);
	}
	disastrOS_semClose(sem);
	disastrOS_exit(0);
}

void main_work(void* arg){
	shared_variable = 0;
	
	printf("Going to start %d threads, each adding %d times %d to a shared variable initialized to zero...", n, m, v); 
	int i;
	
	printf("[%d] opening sem = %d\n", disastrOS_getpid(), id);
	int sem = disastrOS_semOpen(id, mode, value);

	printf("I feel like to spawn %d nice threads\n", n);
	  int alive_children=0;
	  for (i=0; i < n; ++i) {
	    disastrOS_spawn(thread_work, 0);
	    alive_children++;
	  }
	printf("ok\n");
		
	//disastrOS_printStatus();
	  int retval;
	  int pid;
	  while(alive_children>0 && (pid=disastrOS_wait(0, &retval))>=0){ 
	    printf("workFunction, child: %d terminated, retval:%d, alive: %d \n",
	     pid, retval, alive_children);
	    --alive_children;
	    //disastrOS_printStatus();
	  }

	unsigned long int expected_value = (unsigned long int)n*m*v;
	printf("\n\n\nThe value of the shared variable is %lu. It should have been %lu\n\n\n", shared_variable, expected_value);
	if(shared_variable == expected_value) {result[3] = 1;}

	printf("I think it is time to clean up my children mess\n");
	  
	disastrOS_semClose(sem);
	  ListItem* aux=semaphores_list.first;
	  while(aux){
	    Semaphore* r=(Semaphore*)aux;
	    disastrOS_semUnlink(r->id);
	    aux=aux->next;
	  }
	  disastrOS_printStatus();
}


void childOpenCloseWithModeErrors(void* args){
  printf("Hello, I am the child function %d\n",disastrOS_getpid());
  printf("I will open and close a semaphore with mode DSOS_CREATE|DSOS_EXCL\n");
  int mode=DSOS_CREATE|DSOS_EXCL;
  unsigned int value = 1;
  int fd = disastrOS_semOpen(1, mode, value);
  printf("[%d] fd=%d , DSOS_ESEMAPHORECREATE=%d\n", disastrOS_getpid(), fd, DSOS_ESEMAPHORECREATE);
  if(fd == DSOS_ESEMAPHORECREATE) res_1 += 1;
  printf("It is time to sleep a bit\n");
  if(fd < 0)disastrOS_sleep(1);
  else disastrOS_sleep(2);
  int ret = disastrOS_semClose(fd);
  printf("[%d] ret=%d , DSOS_ESEMAPHORECLOSE=%d\n", disastrOS_getpid(), ret, DSOS_ESEMAPHORECLOSE);
  if(fd == DSOS_ESEMAPHORECLOSE) res_1 += 1;
  disastrOS_exit(0);
}


void childTestMaxNumSemaphores(void* args){
  printf("Hello, I am the child function %d\n",disastrOS_getpid());
  printf("I will open a semaphore, wait, sleep, post and finally close it\n");
  int mode=DSOS_CREATE|DSOS_EXCL;
  int id, fd, ret;
  int arr[MAX_NUM_SEMDESCRIPTORS_PER_PROCESS+1] = {-1};
  for (id = 0; id < MAX_NUM_SEMDESCRIPTORS_PER_PROCESS+1; id++){
    fd=disastrOS_semOpen(id, mode, id);
    if (fd < 0) {
	printf("[%d] got error %d (%s=%d)\n", disastrOS_getpid(), fd, "DSOS_ESEMAPHORECREATE", DSOS_ESEMAPHORECREATE);
	res_2 += 1;
    }
    arr[id] = fd;
  }
  for (id = 0; id < MAX_NUM_SEMDESCRIPTORS_PER_PROCESS+1; id++){
    fd = arr[id];
    ret = disastrOS_semClose(fd);
    ret = disastrOS_semUnlink(id);
    if(ret < 0) res_2 += 1;
  }
  disastrOS_exit(0);
}

void childOpensTwiceSemaphoreAndUnlink(void* args){
  printf("Hello, I am the child function %d\n",disastrOS_getpid());
  printf("I will open a semaphore twice, close both descriptors and unlink bothdescriptors\n");
  int mode=DSOS_CREATE;
  int id = 10;
  unsigned int value = 1;

  int fd1=disastrOS_semOpen(id, mode, value);
  int fd2=disastrOS_semOpen(id, mode, value);

  int ret = disastrOS_semUnlink(id);

  if(ret == 0)ret = disastrOS_semClose(fd1);
  if(ret == 0)ret = disastrOS_semClose(fd2);
  if(ret == 0) result[2] = 1;
  disastrOS_exit(0);
}



void testFN(void (*funcp)(void*), int threads, void* args){
  printf("I feel like to spawn %d nice threads\n", threads);
  int alive_children=0;
  for (int i=0; i<threads; ++i) {
    disastrOS_spawn(funcp, 0);
    alive_children++;
  }

  disastrOS_printStatus();
  int retval;
  int pid;
  while(alive_children>0 && (pid=disastrOS_wait(0, &retval))>=0){ 
    printf("initFunction, child: %d terminated, retval:%d, alive: %d \n",
     pid, retval, alive_children);
    --alive_children;
    disastrOS_printStatus();
  }

  printf("I think it is time to clean up my children mess\n");
  
  ListItem* aux=semaphores_list.first;
  while(aux){
    Semaphore* r=(Semaphore*)aux;
    disastrOS_semUnlink(r->id);
    aux=aux->next;
  }
  disastrOS_printStatus();
}

void initFunction(void* args) {
  disastrOS_printStatus();
  printf("hello, I am init and I just started\n");
  disastrOS_spawn(sleeperFunction, 0);

  printf("\n\nTest DSOS_CREATE/DSOS_EXCL mode\n\n\n");
  testFN(childOpenCloseWithModeErrors, 3, args);
  if(res_1 == 2) result[0] = 1;
  printf("\n\nTest max num sems errors with 1 thread\n\n\n");
  testFN(childTestMaxNumSemaphores, 1, args);
  if(res_2 == 2) result[1] = 1;
  printf("\n\nTest 1 sem opened twice by same thread and unlink\n\n\n");
  testFN(childOpensTwiceSemaphoreAndUnlink, 1, args);
  printf("\n\nTest mutual exclusion with 4 threads\n\n\n");
  main_work(args);

  printf("shutdown!\n");
  int res = 0;
  int size = sizeof(result)/sizeof(result[0]);
  for(int y = 0; y < size; y++) res += result[y];
  printf("Successful tests %d of %d\n",res,size);
  disastrOS_shutdown();
}

int main(int argc, char** argv){
  char* logfilename=0;
  if (argc>1) {
    logfilename=argv[1];
  }
  // we create the init process processes
  // the first is in the running variable
  // the others are in the ready queue
  
  // spawn an init process
  printf("start\n");
  disastrOS_start(initFunction, 0, logfilename);
  return 0;
}
