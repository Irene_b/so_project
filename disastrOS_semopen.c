#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

void internal_semOpen(){
	disastrOS_debug("[%d]semOpen: starting\n",disastrOS_getpid());
	// retrieving parameters
	int id = running->syscall_args[0];
	int mode = running->syscall_args[1];
	int value = running->syscall_args[2];

	//check if the id is acceptable
	if(id < 0 || id > MAX_NUM_SEMAPHORES){
		disastrOS_debug("[%d]semOpen: semaphore id not valid\n",disastrOS_getpid());
		running->syscall_retvalue=DSOS_ESEMAPHORECREATE;
	}

	// check if sem_descriptors are exceeding the maximum number
	if(running->sem_descriptors.size + 1 > MAX_NUM_SEMDESCRIPTORS_PER_PROCESS){
		disastrOS_debug("[%d]semOpen: max semaphore descriptors limit exceeded\n",disastrOS_getpid());
		running->syscall_retvalue=DSOS_ESEMAPHORECREATE;
		return;
	}
	
	// check if the semaphore already exists
	Semaphore* sem = SemaphoreList_byId(&semaphores_list, id);

	if(((mode&DSOS_CREATE) == DSOS_CREATE)&&((mode&DSOS_EXCL) == DSOS_EXCL) && sem){
		//the semaphore already exists but the mode include EXCL
		disastrOS_debug("[%d]semOpen: sem already exists and EXCL is used\n",disastrOS_getpid());
		running->syscall_retvalue=DSOS_ESEMAPHORECREATE;
		return;
	}
	else if(!sem && (mode&DSOS_CREATE) == DSOS_CREATE){
		// the semaphore does not exist so i have to create and to add it in the semaphore list
		if (semaphores_list.size + 1 > MAX_NUM_SEMAPHORES){
			// the number of semaphores exceeds the maximum number
			disastrOS_debug("[%d]semOpen: max sem limit exceeded\n",disastrOS_getpid());
			running->syscall_retvalue=DSOS_ESEMAPHORECREATE;
			return;
		}
		sem = Semaphore_alloc(id, value);
		List_insert(&semaphores_list, semaphores_list.last, (ListItem*) sem);
	}

	if(!sem){
		running->syscall_retvalue=DSOS_ESEMAPHORECREATE;
		disastrOS_debug("[%d]semOpen: error opening semaphore with id = %d\n",disastrOS_getpid(),id);
		return;
	}

	// creating the file descriptor and add it in the descriptors list
	SemDescriptor* des = SemDescriptor_alloc(running->last_sem_fd, sem, running);
	if(!des){
		running->syscall_retvalue=DSOS_ESEMAPHORECREATE;
		disastrOS_debug("[%d]semOpen: error opening semaphore descriptor with id = %d ",disastrOS_getpid(),id);
		return;
	}
	List_insert(&running->sem_descriptors, running->sem_descriptors.last, (ListItem*) des);
	// increasing last_sem_fd
	running->last_sem_fd++;

	// create a pointer for the descriptor 
	SemDescriptorPtr* des_ptr = SemDescriptorPtr_alloc(des);

	//connect the pointer to his file descriptor
	des->ptr=des_ptr;

	// insert the descriptor pointer in the semaphore descriptors
	List_insert(&sem->descriptors, sem->descriptors.last, (ListItem*) des_ptr);

	disastrOS_debug("[%d]semOpen: semaphore opened with id = %d and value = %d\n",disastrOS_getpid(),id,sem->count);
	running->syscall_retvalue = des->fd;
	return;
}
