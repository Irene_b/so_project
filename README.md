# disastrOS_semaphores
Implementation of semaphores for the didactic operative system [disastrOS](https://gitlab.com/grisetti/sistemi_operativi_2018_19).

## Project overview
The aim of the project is to provide an implementation of semaphores to control access by multiple processes to a common resource in disastrOS. 

The initial commit is provided by our professor of operative systems Giorgio Grisetti.


### How


#### int disastrOS_semOpen(int semaphore_id, int mode, int value)
At the beginning it will be check that the id given is valid and if the new semaphore will exceed the maximum limit of descriptor.
Then it will be controlled that if DSOS_CREAT and DSOS_EXCL are specified in mode the semaphore can't already exists, if the semaphore does not exist and DSOS_CREAT is specified it will be created a new semaphore with the given id.
At the end SemDescriptor and SemDescriptor_ptr will be created and added to their global lists.
#### int disastrOS_semClose(int id)
By the id it will be searched the SemDescriptor associated, if it is not found an error is returned. Then SemDescriptor and SemDescriptor_ptr will be deallocatd and if it was called semUnlink on this semaphore, the semaphore is deallocated from the system.
#### int disastrOS_semWait(int fd)
By the id it will be searched the SemDescriptor associated, if it is not found an error is returned. Then it will be decremented the semaphore's counter if the value is negative the semaphore will be locked until the semPost unlocked it, otherwise the semWait has finished his work and the semaphore enters in critical section.
#### int disastrOS_semPost(int fd)
By the id it will be searched the SemDescriptor associated, if it is not found an error is returned. Then it will be incremented the semaphore's counter if the value is zero or more a semaphore from the waiting list will be unlocked, otherwise there is no PCB to unlock.

### How to run

```sh
git clone https://gitlab.com/Irene_b/SO_project.git
cd SO_project
make
./disastrOS_test
```
