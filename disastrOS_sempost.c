#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "disastrOS.h"
#include "disastrOS_syscalls.h"
#include "disastrOS_semaphore.h"
#include "disastrOS_semdescriptor.h"

void internal_semPost(){
	disastrOS_debug("[%d]semPost: starting\n",disastrOS_getpid());
	// retrieving parameters
  	int id = running->syscall_args[0];

	SemDescriptor* des = SemDescriptorList_byFd(&running->sem_descriptors, id);
	if(! des){
		// if des is not found there is a problem, the semaphore that have to be use does not exists 
		running->syscall_retvalue=DSOS_ESEMAPHOREPOST;
	}

	Semaphore* sem = des->semaphore;
	assert(sem);
	// increasing the semaphore counter
	sem->count++;

	// if there is a task in the waiting list, this will be remove from the queue
	if(sem->count > 0){
		if(sem->waiting_descriptors.first){
		  	SemDescriptorPtr* des_ptr = (SemDescriptorPtr*) List_detach(&sem->waiting_descriptors, (ListItem*) sem->waiting_descriptors.first);
		  	des_ptr = (SemDescriptorPtr*) List_insert(&sem->descriptors, sem->descriptors.last, (ListItem*) des_ptr);
		  	des = des_ptr->descriptor;
		  	sem->count--;
			PCB* new_running = des->pcb;
			disastrOS_debug("[%d]semPost: PCB with id = %d unlocked\n",disastrOS_getpid(),new_running->pid);
		  	List_detach(&waiting_list, (ListItem*) new_running);
		  	List_insert(&ready_list, ready_list.last, (ListItem*) new_running);
		  	new_running->status = Ready;
		  	new_running->syscall_retvalue = 0;
		}
	}else{
			disastrOS_debug("[%d]semPost: no PCB to unlock\n",disastrOS_getpid());
		
	}
	disastrOS_debug("[%d]semPost: finished\n",disastrOS_getpid());
}
